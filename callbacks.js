const posts=[
    {title:'Post one',body:'This is post one'},
    {title:'Post two',body:'This is post two'},
    {title:'Post three',body:'This is post three'},
];
function getPosts() {
    setTimeout(()=>{
        let output='';
        posts.forEach((post, index)=>{
            output+='<li>${post.title}</li>'
        });
        document.body.innerHTML=output;
    },1000);
}
function createPost(post,callbackfunc){
    setTimeout(()=>{
        posts.push(post);
        callbackfunc();
    },2000);
}
getPosts();
let postObj={title: 'Post Four', body:'This is post four'};
createPost(postObj,getPosts());
